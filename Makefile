COMPOSER = docker run --rm -it -v $$PWD:/app -w /app -u $$(id -u):$$(id -g) composer:2.2
PHP = docker run --rm -it -v $$PWD:/app -w /app -u $$(id -u):$$(id -g) php:8.1

vendor:
	$(COMPOSER) composer install
	touch vendor

example: vendor
	$(PHP) php examples/sendAutomaticEmail.php

test: vendor
	$(PHP) ./vendor/bin/phpunit tests