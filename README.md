Click&Boat - Test at home
---

## Context

Click&Boat sends automated emails to its owners and tenants. It is the AutomaticEmailManager class that takes care of replacing the placeholders of the email templates by the context values.

This class was created a long time ago and has evolved a lot over the years. It has accumulated many bad practices and is difficult to understand and evolve.

Today we want to make a cleanup of this class to make it understandable and evolutive.

## Objectives

The main goal is to make the AutomaticEmailManager class understandable and easier to evolve.

The improvement of other elements of the project will also be taken into account. You are free to add/modify/delete any files to improve code quality and compliance with best practices.

Restriction: you may not modify the implementation of methods if the corresponding PHPDoc indicates this. You can, however, move them to other files if you feel the need.

It's up to you to find the balance between too much and too little.

The indicative time to solve is about 1h30, but it is possible that you finish in more or less time. Take the time you think is appropriate.

## Procedure

### Clone the repo

Your mission, if you accept it, is to clone this project:
- You can then modify the sources of your clone
- Push your code on a **private gitlab repository**
- Create a merge request with changes
- Give access rights to the reviewer assigned to you so that it can review your code
- Indicate in the merge request your main lines of thinking
- Indicate what you would have done if you had more time.

### Deliverable 

Once you consider your resolution complete, you can send an e-mail to tech.hiring@clickandboat.com  with the merge request URL

Good luck and see you soon ;)