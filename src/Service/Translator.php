<?php

use Faker\Factory;

class Translator
{
    use SingletonTrait;

    /**
     * @todo: DON'T MODIFY THIS METHOD
     */
    public function translate(Language $language, string $str): string
    {
        $faker = Factory::create();

        return $faker->sentence();
    }
}